using Laboratorio3Api.Data.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Laboratorio3Api.Controllers
{
    [ApiController]
    [Route("api/tasks/")]
    public class TaskController : ControllerBase 
    {
        private readonly IApiRepository _repo;
        
        public TaskController(IApiRepository repo)
        {
            _repo = repo;
        }

        [HttpGet("get-tasks")]
        public async Task<IActionResult> GetTasks()
        {
            var tasks = await _repo.GetTasksAsync();
            return Ok(tasks);
        }

        [HttpGet("get-by-id-task/{id}")]
        public async Task<IActionResult> GetTaskById(long id)
        {
            var task = await _repo.GetTaskByIdAsync(id);

            if (task == null)
            {
                return NotFound();
            }

            return Ok(task);
        }

        [HttpGet("get-by-title-task/{title}")]
        public async Task<IActionResult> GetTaskByTitle(string title)
        {
            var task = await _repo.GetTaskByTitleAsync(title);

            if (task == null)
            {
                return NotFound();
            }

            return Ok(task);
        }

        [HttpPost("create-task")] 
        public async Task<IActionResult> CreateTask(Laboratorio3Api.Dto.TaskCreateDto taskDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var taskToCreate = new Laboratorio3Api.Models.Task();
            taskToCreate.Title = taskDto.Title;
            taskToCreate.Description = taskDto.Description;
            taskToCreate.CreatedDate = DateTime.Now;
            taskToCreate.Status = 0;

            _repo.Add(taskToCreate);

            if(await _repo.SaveAll())
                return Ok(taskToCreate);
            return BadRequest();
        }

        [HttpDelete("delete-by-id-task/{id}")]
        public async Task<ActionResult<Laboratorio3Api.Models.Task>> Delete(long id)
        {
            var task = await _repo.GetTaskByIdAsync(id);
            if (task == null)
            {
                return NotFound();
            }

            _repo.Delete(task); 

            if (await _repo.SaveAll())
            {
                return task;
            }
            else
            {
                return BadRequest("Failed to delete the task.");
            }
        }

        [HttpPut("update-by-id-task/{id}")]
        public async Task<ActionResult<Laboratorio3Api.Models.Task>> Update(long id, Laboratorio3Api.Dto.TaskUpdateDto taskUpdateDto)
        {
            if(id != taskUpdateDto.Id)
                return BadRequest("The ids do not match");
            
            var taskUpdateToUpdate = await _repo.GetTaskByIdAsync(taskUpdateDto.Id);

            if(taskUpdateToUpdate == null)
                return BadRequest();
            
            taskUpdateToUpdate.FinishDate = DateTime.Now;
            taskUpdateToUpdate.Status = 1;

            if(!await _repo.SaveAll())
            {
                return NoContent();
            }

            return Ok(taskUpdateToUpdate);
        }
    }
}
