using Microsoft.EntityFrameworkCore;

namespace Laboratorio3Api.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<Laboratorio3Api.Models.Task> Tasks{ get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Laboratorio3Api.Models.Task>(builer => {
                builer.HasKey(x => x.Id);
                builer.Property(x => x.Title).HasMaxLength(100);
                builer.Property(x => x.Description).HasMaxLength(300);
                builer.Property(x => x.CreatedDate).HasColumnType("Date")
                    .IsRequired();
                builer.Property(x => x.FinishDate).HasColumnType("Date");
                builer.Property(x => x.Status).HasDefaultValue(0);
            });
        }
    }
}