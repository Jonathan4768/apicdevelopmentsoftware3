using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laboratorio3Api.Data.Interfaces
{
    public interface IApiRepository
    {
        void Add<T>(T entity) where T: class;
        void Delete<T>(T entity) where T: class;
        Task<bool> SaveAll();
        Task<IEnumerable<Laboratorio3Api.Models.Task>> GetTaskAsync();
        Task<Laboratorio3Api.Models.Task> GetTaskByIdAsync(long id);
        Task<Laboratorio3Api.Models.Task> GetTaskByTitleAsync(string title);
        Task<IEnumerable<Laboratorio3Api.Models.Task>> GetTasksAsync();
    }
}