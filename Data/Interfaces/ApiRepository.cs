using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Laboratorio3Api.Data.Interfaces
{
    public class ApiRepository : IApiRepository
    {
        private readonly DataContext _context;

        public ApiRepository(DataContext context)
        {
            _context = context;
        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<IEnumerable<Models.Task>> GetTaskAsync()
        {
            return await _context.Tasks.ToListAsync();
        }

        public async Task<Models.Task> GetTaskByIdAsync(long id)
        {
            return await _context.Tasks.FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<Models.Task> GetTaskByTitleAsync(string title)
        {
            return await _context.Tasks.FirstOrDefaultAsync(u => u.Title == title);
        }


        public async Task<IEnumerable<Models.Task>> GetTasksAsync()
        {
            return await _context.Tasks.ToListAsync();
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}