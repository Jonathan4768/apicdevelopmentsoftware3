namespace Laboratorio3Api.Models
{
    public class Task
    {
        public long Id { get; set; }
        public string Title { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; }
        public DateTime FinishDate { get; set; }
        public int Status { get; set; }
    }
}